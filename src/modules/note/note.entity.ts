import { IsEmpty, IsString } from 'class-validator';
import { Exclude } from 'class-transformer';
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'notes' })
export class NoteEntity {
  @IsEmpty({ always: true, message: 'You cannot change id field' })
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @IsString()
  @ApiProperty({ minLength: 2, maxLength: 255 })
  @Column()
  text: string;

  @Exclude({ toClassOnly: true })
  @ApiProperty()
  @CreateDateColumn({ type: 'timestamp without time zone' })
  createdAt: Date;

  @Exclude({ toClassOnly: true })
  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamp without time zone' })
  updatedAt: Date;
}
