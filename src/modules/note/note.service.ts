import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { NoteEntity } from '@app/modules/note/note.entity';

@Injectable()
export class NoteService extends TypeOrmCrudService<NoteEntity> {
  constructor(
    @InjectRepository(NoteEntity) repo: Repository<NoteEntity>,
  ) {
    super(repo);
  }
}
