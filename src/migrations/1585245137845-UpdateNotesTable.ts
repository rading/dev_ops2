import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateNotesTable1585245137845 implements MigrationInterface {
  name = 'UpdateNotesTable1585245137845';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "notes" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`, undefined);
    await queryRunner.query(`ALTER TABLE "notes" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "notes" DROP COLUMN "updated_at"`, undefined);
    await queryRunner.query(`ALTER TABLE "notes" DROP COLUMN "created_at"`, undefined);
  }

}
