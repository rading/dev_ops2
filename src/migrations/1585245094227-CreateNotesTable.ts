import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateNotesTable1585245094227 implements MigrationInterface {
    name = 'CreateNotesTable1585245094227'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "notes" ("id" SERIAL NOT NULL, "text" character varying NOT NULL, CONSTRAINT "PK_af6206538ea96c4e77e9f400c3d" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "notes"`, undefined);
    }

}
