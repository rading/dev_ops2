import { registerAs } from '@nestjs/config';
import { SnakeNamingStrategy } from '@app/snakeNaming.strategy';

export default registerAs('database', () => ({
  type: 'postgres',
  host: process.env.DB_HOST || 'localhost',
  port: +process.env.DB_PORT || 5432,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  namingStrategy: new SnakeNamingStrategy(),
  logging: process.env.DB_LOGGING === 'true',
  sync: process.env.DB_SYNC === 'true',
  migrationsRun: true,
  entities: ['src/modules/**/*.entity{.ts,.js}'],
  migrations: ['src/migrations/*{.ts,.js}'],
}));
