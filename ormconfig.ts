import * as dotenv from 'dotenv';
import { SnakeNamingStrategy } from './src/snakeNaming.strategy';

dotenv.config({
  path: `.env.${process.env.NODE_ENV || 'development'}`,
});

module.exports = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: +process.env.DB_PORT,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  namingStrategy: new SnakeNamingStrategy(),
  logging: process.env.DB_LOGGING === 'true',
  sync: process.env.DB_SYNC === 'true',
  entities: ['src/modules/**/*.entity{.ts,.js}'],
  migrations: ['src/migrations/*{.ts,.js}'],
};
